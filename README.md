[PublicSuffix]: https://publicsuffix.org
[Nuget]: https://www.nuget.org/
[SLAB]: https://msdn.microsoft.com/en-us/library/dn440729(v=pandp.60).aspx
[DomainNameParser]: https://bitbucket.org/rolosfteverifyshared/components-domainparser-service

# Domain Parser
__.NET domain parsing library.__

## Overview
Parsing domain names is not easy. For example, how does one extract Top Level Domain (TLD) data from the address 'me.com.br'? Is the TLD br, or is it com.br? The correct answer is com.br, but how can we know? The answer is that it is not possible to write simple rules in code to determine TLDs with any accuracy. Therefore, a list of all TLDs is maintained at [PublicSuffix].

The solution to the problem of parsing domains is to maintain a list. The [PublicSuffix] project maintains such a project.

From [PublicSuffix]:

>"Since there is no algorithmic method of finding the highest level at which a domain may be registered for a particular  top-level domain (the policies differ with each registry), the only method is to create a list. This is the aim of the Public Suffix List."

The Rolosoft Domain Parser software parses domain names into TLD, Second Level Domain (SLD) and host using rules found at [PublicSuffix].

## Features and Benefits
* __Thread safe__.
* __High performance__ by using aggressive caching and non-blocking async/await method calls internally.
* __Data redundancy__. Uses online, external live data for up to date list. Automatic failover to local resource copy in remote data source is offline.
* __Async logging__. Non blocking, async logging using [SLAB].
* __Easily extensible__. Need to pull the TLD data from somewhere else (e.g. Azure storage)? No problem, create your own implementation of the built in IReadOnlyRepository<TEntity,TKey> interface and pass this into the factory Create method.

## Who Is This For?
This component is for anyone that needs reliable, accurate and fast domain name parsing.

## Installation
From [Nuget].
```
install-package Rsft.Net.DomainParser
```

## Quickstart
Start parsing domains easily using the internal, default settings.

```c#
/*Create instance of domain parser*/
var domainParser = DomainParserFactory.Create();

/*Parse the domain name*/
var domainInfo = domainParser.Parse("h1.mydomain.com.br");

/*Output results*/
Console.WriteLine("TLD: {0}", domainInfo.Tld);
Console.WriteLine("Second Level Domain: {0}", domainInfo.Sld);
Console.WriteLine("Sub domain: {0}", domainInfo.Subdomain);

/*
* Output:
* TLD: com.br
* Second Level Domain: mydomain
* Sub domain: h1
*/
```

## Extensibility
There are two extensibility options:
* __IReadOnlyRepository<TEntity, in TKey>__. Use this to provide your own implementation of source for the TLD data. Useful if you would like to obtain data from other resource locations (e.g. Azure, SQL etc) other than [PublicSuffix].
* __IConfiguration<Rsft.Net.DomainParser.Entities.Service.Configuration>__. Use this to provide custom configuration options for the URL used to fetch data from [PublicSuffix] or to change the HTTP timeout associated with the fetch.
 
Both extensibility implementations can be supplied to the main factory "Create(..)" method. If none are supplied, internal default implementations are used.

## Logging
High performance, Azure compatible exception and application logging is provided using [SLAB].

Enable logging using standard [SLAB] listeners.
```c#
var ObservableEventListener listener1;
var ObservableEventListener listener2;

listener1 = new ObservableEventListener();
listener1.EnableEvents(ExceptionLoggingEventSource.Log, EventLevel.Error);

listener1.LogToConsole();

listener2 = new ObservableEventListener();
listener2.EnableEvents(ActivityLoggingEventSource.Log, EventLevel.Error, Keywords.All);

listener2.LogToConsole();
```

For full details of logging options see the "Rsft.Net.DomainParser.Diagnostics.EventSources" namespace in the source code.

## Credits
 * [DomainNameParser]