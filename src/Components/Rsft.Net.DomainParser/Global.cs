﻿// <copyright file="Global.cs" company="Rolosoft Ltd">
// Copyright (c) Rolosoft Ltd. All rights reserved.
// </copyright>

namespace Rsft.Net.DomainParser
{
    using Microsoft.Extensions.Logging;

    /// <summary>
    /// Global lifetime objects.
    /// </summary>
    internal static class Global
    {
        /// <summary>
        /// Gets or sets the logger factory.
        /// </summary>
        /// <value>
        /// The logger factory.
        /// </value>
        public static ILoggerFactory LoggerFactory { get; set; }
    }
}