﻿// <copyright file="TaskExtensions.cs" company="Rolosoft Ltd">
// Copyright (c) Rolosoft Ltd. All rights reserved.
// </copyright>

// Copyright 2019 Rolosoft Ltd
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
// http://www.apache.org/licenses/LICENSE-2.0
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
namespace Rsft.Net.DomainParser.Helpers
{
    using System;
    using System.Threading;
    using System.Threading.Tasks;

    /// <summary>
    ///     The task extensions.
    /// </summary>
    internal static class TaskExtensions
    {
        /// <summary>
        /// The timeout after.
        /// </summary>
        /// <param name="task">
        /// The task.
        /// </param>
        /// <param name="millisecondsTimeout">
        /// The milliseconds timeout.
        /// </param>
        /// <typeparam name="TResult">
        /// Type of result.
        /// </typeparam>
        /// <returns>
        /// The <see cref="Task"/> .
        /// </returns>
        public static Task<TResult> TimeoutAfter<TResult>(this Task<TResult> task, int millisecondsTimeout)
        {
            // Short-circuit #1: infinite timeout or task already completed
            if (task.IsCompleted || (millisecondsTimeout == Timeout.Infinite))
            {
                // Either the task has already completed or timeout will never occur.
                // No proxy necessary.
                return task;
            }

            // tcs.Task will be returned as a proxy to the caller
            var tcs = new TaskCompletionSource<TResult>();

            // Short-circuit #2: zero timeout
            if (millisecondsTimeout == 0)
            {
                // We've already timed out.
                tcs.SetException(new TimeoutException());
                return tcs.Task;
            }

            // Set up a timer to complete after the specified timeout period
            var timer = new Timer(
                state =>
                {
                    // Recover our state data
                    var myTcs = (TaskCompletionSource<TResult>)state;

                    // Fault our proxy Task with a TimeoutException
                    myTcs.TrySetException(new TimeoutException());
                },
                tcs,
                millisecondsTimeout,
                Timeout.Infinite);

            // Wire up the logic for what happens when source task completes
            task.ContinueWith(
                (antecedent, state) =>
                {
                    // Recover our state data
                    var tuple = (Tuple<Timer, TaskCompletionSource<TResult>>)state;

                    // Cancel the timer
                    tuple.Item1.Dispose();

                    // Marshal results to proxy
                    MarshalTaskResults(antecedent, tuple.Item2);
                },
                Tuple.Create(timer, tcs),
                CancellationToken.None,
                TaskContinuationOptions.ExecuteSynchronously,
                TaskScheduler.Default);

            return tcs.Task;
        }

        /// <summary>
        /// The timeout after.
        /// </summary>
        /// <param name="task">
        /// The task.
        /// </param>
        /// <param name="millisecondsTimeout">
        /// The milliseconds timeout.
        /// </param>
        /// <returns>
        /// The <see cref="Task"/> .
        /// </returns>
        public static Task TimeoutAfter(this Task task, int millisecondsTimeout)
        {
            // Short-circuit #1: infinite timeout or task already completed
            if (task.IsCompleted || (millisecondsTimeout == Timeout.Infinite))
            {
                // Either the task has already completed or timeout will never occur.
                // No proxy necessary.
                return task;
            }

            // tcs.Task will be returned as a proxy to the caller
            var tcs = new TaskCompletionSource<VoidTypeStruct>();

            // Short-circuit #2: zero timeout
            if (millisecondsTimeout == 0)
            {
                /*We've already timed out.*/
                tcs.SetException(new TimeoutException());
                return tcs.Task;
            }

            // Set up a timer to complete after the specified timeout period
            var timer = new Timer(
                state =>
                {
                    // Recover our state data
                    var myTcs = (TaskCompletionSource<VoidTypeStruct>)state;

                    // Fault our proxy Task with a TimeoutException
                    myTcs.TrySetException(new TimeoutException());
                },
                tcs,
                millisecondsTimeout,
                Timeout.Infinite);

            // Wire up the logic for what happens when source task completes
            task.ContinueWith(
                (antecedent, state) =>
                {
                    // Recover our state data
                    var tuple = (Tuple<Timer, TaskCompletionSource<VoidTypeStruct>>)state;

                    // Cancel the timer
                    tuple.Item1.Dispose();

                    // Marshal results to proxy
                    MarshalTaskResults(antecedent, tuple.Item2);
                },
                Tuple.Create(timer, tcs),
                CancellationToken.None,
                TaskContinuationOptions.ExecuteSynchronously,
                TaskScheduler.Default);

            return tcs.Task;
        }

        /// <summary>
        /// The marshal task results.
        /// </summary>
        /// <param name="source">
        /// The source.
        /// </param>
        /// <param name="proxy">
        /// The proxy.
        /// </param>
        /// <typeparam name="TResult">
        /// The result.
        /// </typeparam>
        private static void MarshalTaskResults<TResult>(Task source, TaskCompletionSource<TResult> proxy)
        {
            switch (source.Status)
            {
                case TaskStatus.Faulted:
                    if (source.Exception != null)
                    {
                        proxy.TrySetException(source.Exception);
                    }

                    break;
                case TaskStatus.Canceled:
                    proxy.TrySetCanceled();
                    break;
                case TaskStatus.RanToCompletion:
                    var castedSource = source as Task<TResult>;
                    proxy.TrySetResult(
                        castedSource == null
                            ? default(TResult)
                            : // source is a Task
                            castedSource.Result); // source is a Task<TResult>
                    break;
            }
        }
    }
}