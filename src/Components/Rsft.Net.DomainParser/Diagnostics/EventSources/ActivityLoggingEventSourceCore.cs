﻿// <copyright file="ActivityLoggingEventSourceCore.cs" company="Rolosoft Ltd">
// Copyright (c) Rolosoft Ltd. All rights reserved.
// </copyright>

// Copyright 2019 Rolosoft Ltd
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
// http://www.apache.org/licenses/LICENSE-2.0
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
namespace Rsft.Net.DomainParser.Diagnostics.EventSources
{
    using System;
    using System.Diagnostics.Tracing;

    using Rsft.Net.DomainParser.Diagnostics.Common;

    /// <summary>
    ///     The activity logging event source.
    /// </summary>
    [EventSource(Name = SourceNames.ActivityLogging)]
    public partial class ActivityLoggingEventSource : EventSource
    {
        /// <summary>
        ///     The instance.
        /// </summary>
        private static readonly Lazy<ActivityLoggingEventSource> Instance =
            new Lazy<ActivityLoggingEventSource>(() => new ActivityLoggingEventSource());

        /// <summary>
        ///     Prevents a default instance of the <see cref="ActivityLoggingEventSource" /> class from being created.
        /// </summary>
        private ActivityLoggingEventSource()
        {
        }

        /// <summary>
        ///     Gets the log.
        /// </summary>
        public static ActivityLoggingEventSource Log => Instance.Value;

        /// <summary>
        ///     The keywords.
        /// </summary>
        public sealed class Keywords
        {
            /// <summary>
            ///     The general diagnostic.
            /// </summary>
            public const EventKeywords GeneralDiagnostic = (EventKeywords)1;

            /// <summary>
            ///     The IO keyword.
            /// </summary>
            public const EventKeywords Io = (EventKeywords)2;

            /// <summary>
            ///     The performance keyword.
            /// </summary>
            public const EventKeywords Perf = (EventKeywords)4;
        }

        /// <summary>
        /// Initializeds the specified module.
        /// </summary>
        /// <param name="module">The module.</param>
        /// <param name="source">The source.</param>
        [Event(
            (int)EventIds.Initialized,
            Message = Messages.Initialized,
            Level = EventLevel.Verbose,
            Keywords = Keywords.GeneralDiagnostic)]
        private void InitializedLocal(string module, string source)
        {
            if (this.IsEnabled(EventLevel.Verbose, Keywords.GeneralDiagnostic))
            {
                this.WriteEvent((int)EventIds.Initialized, module, source);
            }
        }

        /// <summary>
        /// The initializing.
        /// </summary>
        /// <param name="module">
        /// The module.
        /// </param>
        /// <param name="source">
        /// The source.
        /// </param>
        [Event(
            (int)EventIds.Initializing,
            Message = Messages.Initializing,
            Level = EventLevel.Verbose,
            Keywords = Keywords.GeneralDiagnostic)]
        private void InitializingLocal(string module, string source)
        {
            if (this.IsEnabled(EventLevel.Verbose, Keywords.GeneralDiagnostic))
            {
                this.WriteEvent((int)EventIds.Initializing, module, source);
            }
        }

        /// <summary>
        /// The method enter.
        /// </summary>
        /// <param name="module">
        /// The module.
        /// </param>
        /// <param name="source">
        /// The source.
        /// </param>
        [Event(
            (int)EventIds.MethodEnter,
            Message = Messages.MethodEnter,
            Level = EventLevel.Verbose,
            Keywords = Keywords.GeneralDiagnostic)]
        private void MethodEnterLocal(string module, string source)
        {
            if (this.IsEnabled(EventLevel.Verbose, Keywords.GeneralDiagnostic))
            {
                this.WriteEvent((int)EventIds.MethodEnter, module, source);
            }
        }

        /// <summary>
        /// The method exit.
        /// </summary>
        /// <param name="module">
        /// The module.
        /// </param>
        /// <param name="source">
        /// The source.
        /// </param>
        [Event(
            (int)EventIds.MethodExit,
            Message = Messages.MethodExit,
            Level = EventLevel.Verbose,
            Keywords = Keywords.GeneralDiagnostic)]
        private void MethodExitLocal(string module, string source)
        {
            if (this.IsEnabled(EventLevel.Verbose, Keywords.GeneralDiagnostic))
            {
                this.WriteEvent((int)EventIds.MethodExit, module, source);
            }
        }

        /// <summary>
        /// The timer logging.
        /// </summary>
        /// <param name="module">
        /// The module.
        /// </param>
        /// <param name="source">
        /// The source.
        /// </param>
        /// <param name="elapsed">
        /// The elapsed milliseconds.
        /// </param>
        [Event(
            (int)EventIds.TimerLogging,
            Message = Messages.TimerLogging,
            Level = EventLevel.Verbose,
            Keywords = Keywords.Perf)]
        private void TimerLoggingLocal(string module, string source, long elapsed)
        {
            if (this.IsEnabled(EventLevel.Verbose, Keywords.Perf))
            {
                this.WriteEvent((int)EventIds.TimerLogging, module, source, elapsed);
            }
        }

        /// <summary>
        /// HTTPs the get request logging.
        /// </summary>
        /// <param name="module">
        /// The module.
        /// </param>
        /// <param name="source">
        /// The source.
        /// </param>
        /// <param name="url">
        /// The URL.
        /// </param>
        [Event(
            (int)EventIds.HttpGetRequest,
            Message = Messages.HttpGetRequest,
            Level = EventLevel.Verbose,
            Keywords = Keywords.Io)]
        private void HttpGetRequestLoggingLocal(string module, string source, string url)
        {
            if (this.IsEnabled(EventLevel.Verbose, Keywords.Io))
            {
                this.WriteEvent((int)EventIds.HttpGetRequest, module, source, url);
            }
        }

        /// <summary>
        /// The http get response logging.
        /// </summary>
        /// <param name="module">
        /// The module.
        /// </param>
        /// <param name="source">
        /// The source.
        /// </param>
        /// <param name="url">
        /// The URL.
        /// </param>
        /// <param name="response">
        /// The response.
        /// </param>
        [Event(
            (int)EventIds.HttpGetResponse,
            Message = Messages.HttpGetResponse,
            Level = EventLevel.Verbose,
            Keywords = Keywords.Io)]
        private void HttpGetResponseLoggingLocal(string module, string source, string url, string response)
        {
            if (this.IsEnabled(EventLevel.Verbose, Keywords.Io))
            {
                this.WriteEvent((int)EventIds.HttpGetResponse, module, source, url, response);
            }
        }

        /// <summary>
        /// The server response string.
        /// </summary>
        /// <param name="module">
        /// The module.
        /// </param>
        /// <param name="source">
        /// The source.
        /// </param>
        /// <param name="serverResponse">
        /// The server response.
        /// </param>
        [Event(
            (int)EventIds.ServerResponseString,
            Message = Messages.ServerResponseString,
            Level = EventLevel.Verbose,
            Keywords = Keywords.Io)]
        private void ServerResponseStringLocal(string module, string source, string serverResponse)
        {
            if (this.IsEnabled(EventLevel.Verbose, Keywords.Io))
            {
                this.WriteEvent((int)EventIds.ServerResponseString, module, source, serverResponse);
            }
        }

        /// <summary>
        /// The tld rules loaded into cache.
        /// </summary>
        /// <param name="ruleCount">
        /// The rule count.
        /// </param>
        [Event(
            (int)EventIds.TldRulesLoadedIntoCache,
            Message = Messages.TldRulesLoadedIntoCache,
            Level = EventLevel.Verbose)]
        private void TldRulesLoadedIntoCacheLocal(int ruleCount)
        {
            if (this.IsEnabled(EventLevel.Verbose, Keywords.Perf))
            {
                this.WriteEvent((int)EventIds.TldRulesLoadedIntoCache, ruleCount);
            }
        }

        /// <summary>
        /// Services the parsing logging.
        /// </summary>
        /// <param name="query">The query.</param>
        /// <param name="responseTld">The response TLD.</param>
        /// <param name="responseSld">The response SLD.</param>
        /// <param name="responseSubdomain">The response subdomain.</param>
        /// <param name="executionTime">The execution time.</param>
        [Event(
            (int)EventIds.ServiceParsingLogging,
            Message = Messages.ServiceParsingLogging,
            Level = EventLevel.Informational,
            Keywords = Keywords.GeneralDiagnostic)]
        private void ServiceParsingLoggingLocal(
            string query,
            string responseTld,
            string responseSld,
            string responseSubdomain,
            long executionTime)
        {
            if (this.IsEnabled(EventLevel.Informational, Keywords.GeneralDiagnostic))
            {
                this.WriteEvent((int)EventIds.ServiceParsingLogging, query, responseTld, responseSld, responseSubdomain, executionTime);
            }
        }
    }
}