﻿// <copyright file="ExceptionLoggingEventSource.cs" company="Rolosoft Ltd">
// Copyright (c) Rolosoft Ltd. All rights reserved.
// </copyright>

// Copyright 2019 Rolosoft Ltd
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
// http://www.apache.org/licenses/LICENSE-2.0
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
namespace Rsft.Net.DomainParser.Diagnostics.EventSources
{
    using System;
    using System.Diagnostics;
    using System.Diagnostics.Tracing;
    using System.Globalization;
    using Microsoft.Extensions.Logging;
    using Rsft.Net.DomainParser.Diagnostics.Common;

    /// <summary>
    /// The exception logging event source.
    /// </summary>
    public sealed partial class ExceptionLoggingEventSource
    {
        /// <summary>
        ///     The trace source.
        /// </summary>
        private static readonly TraceSource TraceSource = new TraceSource(SourceNames.ExceptionLogging);

        /// <summary>
        /// The critical.
        /// </summary>
        /// <param name="exception">The exception.</param>
        /// <param name="logger">The logger.</param>
        [NonEvent]
        internal void Critical(Exception exception, ILogger logger = null)
        {
            var formatExceptionLogEntry = FormatExceptionLogEntry(
                exception,
                TraceEventType.Critical,
                (int)EventIds.Critical);

            TraceSource.TraceEvent(TraceEventType.Critical, (int)EventIds.Critical, formatExceptionLogEntry);

            logger?.LogCritical((int)EventIds.Critical, exception, formatExceptionLogEntry);

            this.Critical(ModuleName, exception.Source, formatExceptionLogEntry);
        }

        /// <summary>
        /// The critical.
        /// </summary>
        /// <param name="message">The message.</param>
        /// <param name="logger">The logger.</param>
        [NonEvent]
        internal void Critical(string message, ILogger logger = null)
        {
            TraceSource.TraceEvent(TraceEventType.Critical, (int)EventIds.Critical, message);

            logger?.LogCritical((int)EventIds.Critical, message);

            this.Critical(ModuleName, string.Empty, message);
        }

        /// <summary>
        /// The error.
        /// </summary>
        /// <param name="exception">The exception.</param>
        /// <param name="logger">The logger.</param>
        [NonEvent]
        internal void Error(Exception exception, ILogger logger = null)
        {
            var formatExceptionLogEntry = FormatExceptionLogEntry(exception, TraceEventType.Error, (int)EventIds.Error);

            TraceSource.TraceEvent(TraceEventType.Error, (int)EventIds.Error, formatExceptionLogEntry);

            logger?.LogError((int)EventIds.Error, exception, formatExceptionLogEntry);

            this.Error(ModuleName, exception.Source, formatExceptionLogEntry);
        }

        /// <summary>
        /// The error.
        /// </summary>
        /// <param name="message">The message.</param>
        /// <param name="logger">The logger.</param>
        [NonEvent]
        internal void Error(string message, ILogger logger = null)
        {
            TraceSource.TraceEvent(TraceEventType.Error, (int)EventIds.Error, message);

            logger?.LogError((int)EventIds.Error, message);

            this.Error(ModuleName, string.Empty, message);
        }

        /// <summary>
        /// Informations the specified exception.
        /// </summary>
        /// <param name="exception">The exception.</param>
        /// <param name="logger">The logger.</param>
        [NonEvent]
        internal void Information(Exception exception, ILogger logger = null)
        {
            var formatExceptionLogEntry = FormatExceptionLogEntry(
                exception,
                TraceEventType.Information,
                (int)EventIds.Informational);

            TraceSource.TraceEvent(TraceEventType.Information, (int)EventIds.Informational, formatExceptionLogEntry);

            logger?.LogInformation((int)EventIds.Informational, exception, formatExceptionLogEntry);

            this.Information(ModuleName, exception.Source, formatExceptionLogEntry);
        }

        /// <summary>
        /// The information.
        /// </summary>
        /// <param name="message">The message.</param>
        /// <param name="logger">The logger.</param>
        [NonEvent]
        internal void Information(string message, ILogger logger = null)
        {
            TraceSource.TraceEvent(TraceEventType.Information, (int)EventIds.Informational, message);

            logger?.LogInformation((int)EventIds.Informational, message);

            this.Information(ModuleName, string.Empty, message);
        }

        /// <summary>
        /// Verboses the specified exception.
        /// </summary>
        /// <param name="exception">The exception.</param>
        /// <param name="logger">The logger.</param>
        [NonEvent]
        internal void Verbose(Exception exception, ILogger logger = null)
        {
            var formatExceptionLogEntry = FormatExceptionLogEntry(
                exception,
                TraceEventType.Verbose,
                (int)EventIds.Verbose);

            TraceSource.TraceEvent(TraceEventType.Verbose, (int)EventIds.Verbose, formatExceptionLogEntry);

            logger?.LogTrace((int)EventIds.Verbose, exception, formatExceptionLogEntry);

            this.Verbose(ModuleName, exception.Source, formatExceptionLogEntry);
        }

        /// <summary>
        /// The verbose.
        /// </summary>
        /// <param name="message">The message.</param>
        /// <param name="logger">The logger.</param>
        [NonEvent]
        internal void Verbose(string message, ILogger logger = null)
        {
            TraceSource.TraceEvent(TraceEventType.Verbose, (int)EventIds.Verbose, message);

            logger?.LogTrace((int)EventIds.Verbose, message);

            this.Verbose(ModuleName, string.Empty, message);
        }

        /// <summary>
        /// The warning.
        /// </summary>
        /// <param name="exception">The exception.</param>
        /// <param name="logger">The logger.</param>
        [NonEvent]
        internal void Warning(Exception exception, ILogger logger = null)
        {
            var formatExceptionLogEntry = FormatExceptionLogEntry(
                exception,
                TraceEventType.Warning,
                (int)EventIds.Warning);

            TraceSource.TraceEvent(TraceEventType.Warning, (int)EventIds.Warning, formatExceptionLogEntry);

            logger?.LogWarning((int)EventIds.Warning, exception, formatExceptionLogEntry);

            this.Warning(ModuleName, exception.Source, formatExceptionLogEntry);
        }

        /// <summary>
        /// Warnings the specified message.
        /// </summary>
        /// <param name="message">The message.</param>
        /// <param name="logger">The logger.</param>
        [NonEvent]
        internal void Warning(string message, ILogger logger = null)
        {
            TraceSource.TraceEvent(TraceEventType.Warning, (int)EventIds.Warning, message);

            logger?.LogWarning((int)EventIds.Warning, message);

            this.Warning(ModuleName, string.Empty, message);
        }

        /// <summary>
        /// Warnings the remote resource unavailable.
        /// </summary>
        /// <param name="remoteResource">The remote resource.</param>
        /// <param name="message">The message.</param>
        /// <param name="logger">The logger.</param>
        [NonEvent]
        internal void WarningRemoteResourceUnavailable(string remoteResource, string message, ILogger logger = null)
        {
            var formatLogEntry = string.Format(
                CultureInfo.InvariantCulture,
                Messages.WarningRemoteResourceUnavailable,
                ModuleName,
                string.Empty,
                remoteResource,
                message);

            TraceSource.TraceEvent(
                TraceEventType.Warning,
                (int)EventIds.WarningRemoteResourceUnavailable,
                formatLogEntry);

            logger?.LogWarning((int)EventIds.Warning, formatLogEntry);

            this.WarningRemoteResourceUnavailable(ModuleName, string.Empty, remoteResource, message);
        }
    }
}