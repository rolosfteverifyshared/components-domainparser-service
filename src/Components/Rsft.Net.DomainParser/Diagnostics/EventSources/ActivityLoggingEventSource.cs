﻿// <copyright file="ActivityLoggingEventSource.cs" company="Rolosoft Ltd">
// Copyright (c) Rolosoft Ltd. All rights reserved.
// </copyright>

// Copyright 2019 Rolosoft Ltd
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
// http://www.apache.org/licenses/LICENSE-2.0
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
namespace Rsft.Net.DomainParser.Diagnostics.EventSources
{
    using System.Diagnostics.Tracing;
    using Microsoft.Extensions.Logging;
    using Rsft.Net.DomainParser.Diagnostics.Common;

    /// <summary>
    /// The activity logging event source.
    /// </summary>
    public partial class ActivityLoggingEventSource
    {
        /// <summary>
        /// Initializeds the specified module.
        /// </summary>
        /// <param name="module">The module.</param>
        /// <param name="source">The source.</param>
        /// <param name="logger">The logger.</param>
        [NonEvent]
        public void Initialized(string module, string source, ILogger logger = null)
        {
            logger?.LogDebug((int)EventIds.Initialized, Messages.Initialized, module, source);

            this.InitializedLocal(module, source);
        }

        /// <summary>
        /// Initializings the specified module.
        /// </summary>
        /// <param name="module">The module.</param>
        /// <param name="source">The source.</param>
        /// <param name="logger">The logger.</param>
        [NonEvent]
        public void Initializing(string module, string source, ILogger logger = null)
        {
            logger?.LogDebug((int)EventIds.Initializing, Messages.Initializing, module, source);
            this.InitializingLocal(module, source);
        }

        /// <summary>
        /// The method enter.
        /// </summary>
        /// <param name="module">The module.</param>
        /// <param name="source">The source.</param>
        /// <param name="logger">The logger.</param>
        [NonEvent]
        public void MethodEnter(string module, string source, ILogger logger = null)
        {
            logger?.LogDebug((int)EventIds.MethodEnter, Messages.MethodEnter, module, source);
            this.MethodEnterLocal(module, source);
        }

        /// <summary>
        /// The method exit.
        /// </summary>
        /// <param name="module">The module.</param>
        /// <param name="source">The source.</param>
        /// <param name="logger">The logger.</param>
        [NonEvent]
        public void MethodExit(string module, string source, ILogger logger = null)
        {
            logger?.LogDebug((int)EventIds.MethodExit, Messages.MethodExit, module, source);
            this.MethodExitLocal(module, source);
        }

        /// <summary>
        /// The timer logging.
        /// </summary>
        /// <param name="module">The module.</param>
        /// <param name="source">The source.</param>
        /// <param name="elapsed">The elapsed milliseconds.</param>
        /// <param name="logger">The logger.</param>
        [NonEvent]
        public void TimerLogging(string module, string source, long elapsed, ILogger logger = null)
        {
            logger?.LogDebug((int)EventIds.TimerLogging, Messages.TimerLogging, module, source, elapsed);

            this.TimerLoggingLocal(module, source, elapsed);
        }

        /// <summary>
        /// HTTPs the get request logging.
        /// </summary>
        /// <param name="module">The module.</param>
        /// <param name="source">The source.</param>
        /// <param name="url">The URL.</param>
        /// <param name="logger">The logger.</param>
        [NonEvent]
        public void HttpGetRequestLogging(string module, string source, string url, ILogger logger = null)
        {
            logger?.LogDebug((int)EventIds.HttpGetRequest, Messages.HttpGetRequest, module, source, url);

            this.HttpGetRequestLoggingLocal(module, source, url);
        }

        /// <summary>
        /// The http get response logging.
        /// </summary>
        /// <param name="module">The module.</param>
        /// <param name="source">The source.</param>
        /// <param name="url">The URL.</param>
        /// <param name="response">The response.</param>
        /// <param name="logger">The logger.</param>
        [NonEvent]
        public void HttpGetResponseLogging(string module, string source, string url, string response, ILogger logger = null)
        {
            logger?.LogDebug((int)EventIds.HttpGetResponse, Messages.HttpGetResponse, module, source, url, response);

            this.HttpGetResponseLoggingLocal(module, source, url, response);
        }

        /// <summary>
        /// The server response string.
        /// </summary>
        /// <param name="module">The module.</param>
        /// <param name="source">The source.</param>
        /// <param name="serverResponse">The server response.</param>
        /// <param name="logger">The logger.</param>
        [NonEvent]
        public void ServerResponseString(string module, string source, string serverResponse, ILogger logger = null)
        {
            logger?.LogDebug((int)EventIds.ServerResponseString, Messages.ServerResponseString, module, source, serverResponse);
            this.ServerResponseStringLocal(module, source, serverResponse);
        }

        /// <summary>
        /// The tld rules loaded into cache.
        /// </summary>
        /// <param name="ruleCount">The rule count.</param>
        /// <param name="logger">The logger.</param>
        [NonEvent]
        public void TldRulesLoadedIntoCache(int ruleCount, ILogger logger = null)
        {
            logger?.LogDebug((int)EventIds.TldRulesLoadedIntoCache, Messages.TldRulesLoadedIntoCache, ruleCount);
            this.TldRulesLoadedIntoCacheLocal(ruleCount);
        }

        /// <summary>
        /// Services the parsing logging.
        /// </summary>
        /// <param name="query">The query.</param>
        /// <param name="responseTld">The response TLD.</param>
        /// <param name="responseSld">The response SLD.</param>
        /// <param name="responseSubdomain">The response subdomain.</param>
        /// <param name="executionTime">The execution time.</param>
        /// <param name="logger">The logger.</param>
        [NonEvent]
        public void ServiceParsingLogging(
            string query,
            string responseTld,
            string responseSld,
            string responseSubdomain,
            long executionTime,
            ILogger logger = null)
        {
            logger?.LogDebug((int)EventIds.ServiceParsingLogging, Messages.ServiceParsingLogging, query, responseTld, responseSld, responseSubdomain, executionTime);

            this.ServiceParsingLoggingLocal(query, responseTld, responseSld, responseSubdomain, executionTime);
        }
    }
}