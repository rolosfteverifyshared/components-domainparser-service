﻿// <copyright file="DomainParser.cs" company="Rolosoft Ltd">
// Copyright (c) Rolosoft Ltd. All rights reserved.
// </copyright>

namespace Rsft.Net.DomainParser.Service
{
    using System.Diagnostics;

    using JetBrains.Annotations;
    using Microsoft.Extensions.Logging;
    using Rsft.Net.DomainParser.Core.Entities.Service;
    using Rsft.Net.DomainParser.Core.Interfaces;

    /// <summary>
    /// The domain parser.
    /// </summary>
    internal sealed class DomainParser : IDomainParser<DomainInfo>
    {
        /// <summary>
        ///     The logging module name.
        /// </summary>
        private const string LoggingSourceName = @"Rsft.Net.DomainParser.Service.DomainParser";

        /// <summary>
        /// The domain parser.
        /// </summary>
        [NotNull]
        private readonly IDomainParser<Rsft.Net.DomainParser.Core.Entities.Logic.DomainInfo> domainParser;

        /// <summary>
        /// The logger.
        /// </summary>
        [NotNull]
        private readonly ILogger logger;

        /// <summary>
        /// Initializes a new instance of the <see cref="DomainParser" /> class.
        /// </summary>
        /// <param name="domainParser">The domain parser.</param>
        /// <param name="loggerFactory">The logger factory.</param>
        public DomainParser([NotNull] IDomainParser<Rsft.Net.DomainParser.Core.Entities.Logic.DomainInfo> domainParser, [NotNull] ILoggerFactory loggerFactory)
        {
            this.logger = loggerFactory.CreateLogger<DomainParser>();

            this.domainParser = domainParser;
        }

        /// <summary>
        /// The parse.
        /// </summary>
        /// <param name="domainString">
        /// The domain string.
        /// </param>
        /// <returns>
        /// The <see cref="DomainInfo"/>.
        /// </returns>
        public DomainInfo Parse(string domainString)
        {
            Diagnostics.EventSources.ActivityLoggingEventSource.Log.MethodEnter("Parse(string domainString)", LoggingSourceName, this.logger);

            var stopwatch = Stopwatch.StartNew();
            var domainInfo = this.domainParser.Parse(domainString);
            stopwatch.Stop();

            Diagnostics.EventSources.ActivityLoggingEventSource.Log.ServiceParsingLogging(domainString, domainInfo.Tld, domainInfo.Sld, domainInfo.Subdomain, stopwatch.ElapsedMilliseconds);

            Diagnostics.EventSources.ActivityLoggingEventSource.Log.MethodExit("Parse(string domainString)", LoggingSourceName, this.logger);

            /*Map*/
            return new DomainInfo { Sld = domainInfo.Sld, Tld = domainInfo.Tld, Subdomain = domainInfo.Subdomain };
        }

        /// <summary>
        /// The try parse.
        /// </summary>
        /// <param name="domainString">
        /// The domain string.
        /// </param>
        /// <param name="response">
        /// The response.
        /// </param>
        /// <returns>
        /// The <see cref="bool"/>.
        /// </returns>
        public bool TryParse(string domainString, out DomainInfo response)
        {
            Diagnostics.EventSources.ActivityLoggingEventSource.Log.MethodEnter("TryParse(string domainString, out DomainInfo response)", LoggingSourceName, this.logger);

            var tryParse = false;

            var stopwatch = Stopwatch.StartNew();

            try
            {
                tryParse = this.domainParser.TryParse(domainString, out Rsft.Net.DomainParser.Core.Entities.Logic.DomainInfo fromLogic);

                /*Map*/
                response = new DomainInfo { Sld = fromLogic.Sld, Tld = fromLogic.Tld, Subdomain = fromLogic.Subdomain };

                if (tryParse)
                {
                    Diagnostics.EventSources.ActivityLoggingEventSource.Log.ServiceParsingLogging(
                        domainString,
                        fromLogic.Tld,
                        fromLogic.Sld,
                        fromLogic.Subdomain,
                        stopwatch.ElapsedMilliseconds,
                        this.logger);
                }
            }
            catch
            {
                // don't care
                response = null;
            }

            stopwatch.Stop();

            Diagnostics.EventSources.ActivityLoggingEventSource.Log.MethodExit("TryParse(string domainString, out DomainInfo response)", LoggingSourceName, this.logger);

            return tryParse;
        }
    }
}