﻿// <copyright file="DomainParserFactory.cs" company="Rolosoft Ltd">
// Copyright (c) Rolosoft Ltd. All rights reserved.
// </copyright>

namespace Rsft.Net.DomainParser
{
    using System;
    using Microsoft.Extensions.Logging;
    using Rsft.Net.DomainParser.Core.Entities.Logic;
    using Rsft.Net.DomainParser.Core.Entities.Logic.Configuration;
    using Rsft.Net.DomainParser.Core.Entities.Service;
    using Rsft.Net.DomainParser.Core.Interfaces;
    using Rsft.Net.DomainParser.Logic;
    using Rsft.Net.DomainParser.Logic.Configuration;
    using Rsft.Net.DomainParser.Logic.Repository;
    using DomainInfo = Rsft.Net.DomainParser.Core.Entities.Service.DomainInfo;

    /// <summary>
    ///     The domain parser factory.
    /// </summary>
    public static class DomainParserFactory
    {
        /// <summary>
        ///     The default public URL data timeout.
        /// </summary>
        private const int DefaultPublicUrlDataTimeout = 8000;

        /// <summary>
        ///     The default public URL.
        /// </summary>
        private const string DefaultPublicUrl = @"https://publicsuffix.org/list/effective_tld_names.dat";

        /// <summary>
        ///     The default logic configuration.
        /// </summary>
        private static readonly Lazy<IConfiguration<DomainParserConfiguration>> DefaultLogicConfiguration =
            new Lazy<IConfiguration<DomainParserConfiguration>>(() => new DefaultConfiguration());

        /// <summary>
        ///     The TLD rule resolver.
        /// </summary>
        private static readonly Lazy<ITldRuleResolver<TldRule>> TldRuleResolver =
            new Lazy<ITldRuleResolver<TldRule>>(() => new TldRuleResolverPublicSuffix());

        private static Lazy<IReadOnlyRepository<string, string>> defaultLogicReadOnlyLocalResourceRepo;

        /// <summary>
        ///     The default logic read only public resource repo.
        /// </summary>
        private static Lazy<IReadOnlyRepository<string, string>> defaultLogicReadOnlyPublicResourceRepo;

        /// <summary>
        ///     The custom user logic configuration.
        /// </summary>
        private static Lazy<IConfiguration<DomainParserConfiguration>> customUserLogicConfiguration;

        /// <summary>
        ///     The custom user logic read only public resource repo.
        /// </summary>
        private static Lazy<IReadOnlyRepository<string, string>> customUserLogicReadOnlyPublicResourceRepo;

        /// <summary>
        ///     The TLD rules cache.
        /// </summary>
        private static Lazy<ITldRulesCache> tldRulesCache;

        /// <summary>
        ///     The default logic domain parser.
        /// </summary>
        private static Lazy<IDomainParser<Rsft.Net.DomainParser.Core.Entities.Logic.DomainInfo>> defaultLogicDomainParser;

        /// <summary>
        ///     The service domain parser.
        /// </summary>
        private static Lazy<IDomainParser<DomainInfo>> serviceDomainParser;

        /// <summary>
        /// The create.
        /// </summary>
        /// <param name="configuration">The configuration. Uses default, internal configuration if not specified.</param>
        /// <param name="customReadOnlyRepositoryExternalPublicList">The custom read only repository external public list. Uses
        /// default, internal repository (reading https://publicsuffix.org/list/effective_tld_names.dat) if not specified.</param>
        /// <param name="loggerFactory">The logger factory.</param>
        /// <returns>
        /// The domain parser.
        /// </returns>
        public static IDomainParser<DomainInfo> Create(
            IConfiguration<Configuration> configuration = null,
            IReadOnlyRepository<string, string> customReadOnlyRepositoryExternalPublicList = null,
            ILoggerFactory loggerFactory = null)
        {
            var loggerFactoryToUse = loggerFactory ?? new LoggerFactory();

            Global.LoggerFactory = loggerFactoryToUse;

            if (defaultLogicReadOnlyLocalResourceRepo == null)
            {
                defaultLogicReadOnlyLocalResourceRepo = new Lazy<IReadOnlyRepository<string, string>>(() => new LocalResourceRepository(Global.LoggerFactory));
            }

            if (defaultLogicReadOnlyPublicResourceRepo == null)
            {
                defaultLogicReadOnlyPublicResourceRepo = new Lazy<IReadOnlyRepository<string, string>>(
                    () => new PublicSuffixRepository(DefaultLogicConfiguration.Value, Global.LoggerFactory));
            }

            /*configuration is null*/
            if (configuration == null)
            {
                /*custom repo is null*/
                if (customReadOnlyRepositoryExternalPublicList == null)
                {
                    tldRulesCache =
                        new Lazy<ITldRulesCache>(
                            () =>
                            new TldRulesCache(
                                DefaultLogicConfiguration.Value,
                                TldRuleResolver.Value,
                                defaultLogicReadOnlyLocalResourceRepo.Value,
                                defaultLogicReadOnlyPublicResourceRepo.Value,
                                Global.LoggerFactory));

                    defaultLogicDomainParser = new Lazy<IDomainParser<Rsft.Net.DomainParser.Core.Entities.Logic.DomainInfo>>(() => new Rsft.Net.DomainParser.Logic.DomainParser(tldRulesCache.Value, Global.LoggerFactory));

                    serviceDomainParser =
                        new Lazy<IDomainParser<DomainInfo>>(
                            () => new Service.DomainParser(defaultLogicDomainParser.Value, Global.LoggerFactory));

                    return serviceDomainParser.Value;
                }

                /*configuration is null, custom user repo is defined*/
                customUserLogicReadOnlyPublicResourceRepo =
                    new Lazy<IReadOnlyRepository<string, string>>(() => customReadOnlyRepositoryExternalPublicList);

                tldRulesCache =
                        new Lazy<ITldRulesCache>(
                            () =>
                            new TldRulesCache(
                                DefaultLogicConfiguration.Value,
                                TldRuleResolver.Value,
                                defaultLogicReadOnlyLocalResourceRepo.Value,
                                customReadOnlyRepositoryExternalPublicList,
                                Global.LoggerFactory));

                defaultLogicDomainParser = new Lazy<IDomainParser<Rsft.Net.DomainParser.Core.Entities.Logic.DomainInfo>>(() => new DomainParser(tldRulesCache.Value, Global.LoggerFactory));

                serviceDomainParser =
                        new Lazy<IDomainParser<DomainInfo>>(
                            () => new Service.DomainParser(defaultLogicDomainParser.Value, Global.LoggerFactory));

                return serviceDomainParser.Value;
            }

            /*configuration is defined*/
            var domainParserConfiguration = new DomainParserConfiguration
                                                {
                                                    TldPublicFetchTimeout =
                                                        configuration.Configuration
                                                            .TldPublicFetchTimeout < 1
                                                            ? DefaultPublicUrlDataTimeout
                                                            : configuration.Configuration
                                                                  .TldPublicFetchTimeout,
                                                    TldPublicRulesUrl =
                                                        string.IsNullOrWhiteSpace(
                                                            configuration.Configuration
                                                            .TldPublicRulesUrl)
                                                            ? DefaultPublicUrl
                                                            : configuration.Configuration
                                                                  .TldPublicRulesUrl,
                                                };

            customUserLogicConfiguration =
                new Lazy<IConfiguration<DomainParserConfiguration>>(
                    () => new UserConfiguredConfiguration { Configuration = domainParserConfiguration });

            /*configuration defined, custom external repo is null*/
            if (customReadOnlyRepositoryExternalPublicList == null)
            {
                customUserLogicReadOnlyPublicResourceRepo =
                    new Lazy<IReadOnlyRepository<string, string>>(() => new PublicSuffixRepository(customUserLogicConfiguration.Value, Global.LoggerFactory));

                tldRulesCache =
                    new Lazy<ITldRulesCache>(
                        () =>
                        new TldRulesCache(
                            customUserLogicConfiguration.Value,
                            TldRuleResolver.Value,
                            defaultLogicReadOnlyLocalResourceRepo.Value,
                            customUserLogicReadOnlyPublicResourceRepo.Value,
                            Global.LoggerFactory));

                defaultLogicDomainParser =
                    new Lazy<IDomainParser<Rsft.Net.DomainParser.Core.Entities.Logic.DomainInfo>>(() => new DomainParser(tldRulesCache.Value, Global.LoggerFactory));

                serviceDomainParser =
                    new Lazy<IDomainParser<DomainInfo>>(
                        () => new Service.DomainParser(defaultLogicDomainParser.Value, Global.LoggerFactory));

                return serviceDomainParser.Value;
            }

            /*configuration defined, custom external repo defined*/
            customUserLogicReadOnlyPublicResourceRepo =
                    new Lazy<IReadOnlyRepository<string, string>>(() => customReadOnlyRepositoryExternalPublicList);

            tldRulesCache =
                        new Lazy<ITldRulesCache>(
                            () =>
                            new TldRulesCache(
                                customUserLogicConfiguration.Value,
                                TldRuleResolver.Value,
                                defaultLogicReadOnlyLocalResourceRepo.Value,
                                customUserLogicReadOnlyPublicResourceRepo.Value,
                                Global.LoggerFactory));

            defaultLogicDomainParser = new Lazy<IDomainParser<Rsft.Net.DomainParser.Core.Entities.Logic.DomainInfo>>(() => new DomainParser(tldRulesCache.Value, Global.LoggerFactory));

            serviceDomainParser =
                    new Lazy<IDomainParser<DomainInfo>>(
                        () => new Service.DomainParser(defaultLogicDomainParser.Value, Global.LoggerFactory));

            return serviceDomainParser.Value;
        }
    }
}