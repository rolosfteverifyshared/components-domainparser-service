﻿// <copyright file="PublicSuffixRepository.cs" company="Rolosoft Ltd">
// Copyright (c) Rolosoft Ltd. All rights reserved.
// </copyright>

namespace Rsft.Net.DomainParser.Logic.Repository
{
    using System;
    using System.Collections.Generic;
    using System.Diagnostics;
    using System.IO;
    using System.Net.Http;
    using System.Text;
    using System.Threading;
    using System.Threading.Tasks;
    using JetBrains.Annotations;
    using Microsoft.Extensions.Logging;
    using Rsft.Net.DomainParser.Core.Entities.Logic.Configuration;
    using Rsft.Net.DomainParser.Core.Interfaces;
    using Rsft.Net.DomainParser.Diagnostics.EventSources;
    using Rsft.Net.DomainParser.Helpers;

    /// <summary>
    /// The public suffix repository.
    /// </summary>
    internal sealed class PublicSuffixRepository : RepositoryBase
    {
        /// <summary>
        ///     The logging module name.
        /// </summary>
        private const string LoggingSourceName = @"Rsft.Net.DomainParser.Logic.Repository.PublicSuffixRepository";

        /// <summary>
        /// The configuration.
        /// </summary>
        [NotNull]
        private readonly IConfiguration<DomainParserConfiguration> configuration;

        /// <summary>
        /// The logger.
        /// </summary>
        [NotNull]
        private readonly ILogger logger;

        /// <summary>
        /// Initializes a new instance of the <see cref="PublicSuffixRepository" /> class.
        /// </summary>
        /// <param name="configuration">The configuration.</param>
        /// <param name="loggerFactory">The logger factory.</param>
        public PublicSuffixRepository(IConfiguration<DomainParserConfiguration> configuration, ILoggerFactory loggerFactory)
            : base(loggerFactory)
        {
            this.configuration = configuration;
            this.logger = loggerFactory.CreateLogger<PublicSuffixRepository>();
        }

        /// <summary>
        /// The get all async.
        /// </summary>
        /// <param name="cancellationToken">
        /// The cancellation token.
        /// </param>
        /// <returns>
        /// The <see cref="Task"/>.
        /// </returns>
        /// <exception cref="TimeoutException">Operation timed-out wrapped in AggregateException.</exception>
        public override async Task<IEnumerable<string>> GetAllAsync(CancellationToken cancellationToken)
        {
            ActivityLoggingEventSource.Log.MethodEnter(
                "GetAllAsync(CancellationToken cancellationToken)",
                LoggingSourceName,
                this.logger);

            var stopwatch = Stopwatch.StartNew();

            using (var client = new HttpClient())
            {
                using (
                    var datFile =
                        await
                        client.GetStreamAsync(this.configuration.Configuration.TldPublicRulesUrl)
                            .TimeoutAfter(this.configuration.Configuration.TldPublicFetchTimeout)
                            .ConfigureAwait(false))
                {
                    var data = new List<string>();

                    using (var reader = new StreamReader(datFile, Encoding.UTF8))
                    {
                        try
                        {
                            string line;
                            while (
                                (line =
                                 await
                                 reader.ReadLineAsync()
                                     .TimeoutAfter(this.configuration.Configuration.TldPublicFetchTimeout)
                                     .ConfigureAwait(false)) != null)
                            {
                                data.Add(line);
                            }
                        }
                        catch (AggregateException aggregateException)
                        {
                            aggregateException.Handle(
                                e =>
                                {
                                    ExceptionLoggingEventSource.Log.Error(e);
                                    return false;
                                });
                        }

                        var @join = string.Join(Environment.NewLine, data);

                        ActivityLoggingEventSource.Log.ServerResponseString(
                            "ReadPublicRulesAsync",
                            LoggingSourceName,
                            @join);

                        stopwatch.Stop();

                        ActivityLoggingEventSource.Log.TimerLogging(
                            "GetAllAsync(CancellationToken cancellationToken)",
                            LoggingSourceName,
                            stopwatch.ElapsedMilliseconds,
                            this.logger);
                        ActivityLoggingEventSource.Log.MethodExit(
                            "GetAllAsync(CancellationToken cancellationToken)",
                            LoggingSourceName,
                            this.logger);

                        return data;
                    }
                }
            }
        }
    }
}