﻿// <copyright file="RepositoryBase.cs" company="Rolosoft Ltd">
// Copyright (c) Rolosoft Ltd. All rights reserved.
// </copyright>

namespace Rsft.Net.DomainParser.Logic.Repository
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Threading;
    using System.Threading.Tasks;

    using JetBrains.Annotations;
    using Microsoft.Extensions.Logging;
    using Rsft.Net.DomainParser.Core.Interfaces;

    /// <summary>
    /// The repository base.
    /// </summary>
    internal abstract class RepositoryBase : IReadOnlyRepository<string, string>
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="RepositoryBase"/> class.
        /// </summary>
        /// <param name="loggerFactory">The logger factory.</param>
        protected RepositoryBase([NotNull]ILoggerFactory loggerFactory)
        {
        }

        /// <summary>
        /// The get all.
        /// </summary>
        /// <returns>
        /// The IEnumerable of string.
        /// </returns>
        public IEnumerable<string> GetAll()
        {
            return this.GetAllAsync(CancellationToken.None).Result;
        }

        /// <summary>
        /// The get all async.
        /// </summary>
        /// <param name="cancellationToken">
        /// The cancellation token.
        /// </param>
        /// <returns>
        /// The <see cref="Task"/>.
        /// </returns>
        public abstract Task<IEnumerable<string>> GetAllAsync(CancellationToken cancellationToken);

        /// <summary>
        /// The get.
        /// </summary>
        /// <param name="key">
        /// The key.
        /// </param>
        /// <returns>
        /// The <see cref="string"/>.
        /// </returns>
        public string Get(string key)
        {
            return
                this.GetAll()
                    .AsQueryable()
                    .FirstOrDefault(r => string.Compare(r, key, StringComparison.OrdinalIgnoreCase) == 0);
        }

        /// <summary>
        /// The get async.
        /// </summary>
        /// <param name="key">
        /// The key.
        /// </param>
        /// <param name="cancellationToken">
        /// The cancellation token.
        /// </param>
        /// <returns>
        /// The <see cref="Task"/>.
        /// </returns>
        public async Task<string> GetAsync(string key, CancellationToken cancellationToken)
        {
            var result = await this.GetAllAsync(cancellationToken).ConfigureAwait(false);

            return
                result.AsQueryable()
                    .FirstOrDefault(r => string.Compare(r, key, StringComparison.OrdinalIgnoreCase) == 0);
        }
    }
}