﻿// <copyright file="LocalResourceRepository.cs" company="Rolosoft Ltd">
// Copyright (c) Rolosoft Ltd. All rights reserved.
// </copyright>

namespace Rsft.Net.DomainParser.Logic.Repository
{
    using System.Collections.Generic;
    using System.Diagnostics.Contracts;
    using System.IO;
    using System.Threading;
    using System.Threading.Tasks;
    using JetBrains.Annotations;
    using Microsoft.Extensions.Logging;
    using Rsft.Net.DomainParser.Diagnostics.EventSources;
    using Rsft.Net.DomainParser.Logic.Data;

    /// <summary>
    /// The local resource repository.
    /// </summary>
    internal sealed class LocalResourceRepository : RepositoryBase
    {
        /// <summary>
        ///     The logging module name.
        /// </summary>
        private const string LoggingSourceName = @"Rsft.Net.DomainParser.Logic.Repository.LocalResourceRepository";

        /// <summary>
        /// The logger.
        /// </summary>
        [NotNull]
        private readonly ILogger logger;

        /// <summary>
        /// Initializes a new instance of the <see cref="LocalResourceRepository"/> class.
        /// </summary>
        /// <param name="loggerFactory">The logger factory.</param>
        public LocalResourceRepository([NotNull] ILoggerFactory loggerFactory)
            : base(loggerFactory)
        {
            Contract.Requires(loggerFactory != null);

            this.logger = loggerFactory.CreateLogger<LocalResourceRepository>();
        }

        /// <summary>
        /// The get all async.
        /// </summary>
        /// <param name="cancellationToken">
        /// The cancellation token.
        /// </param>
        /// <returns>
        /// The <see cref="Task"/>.
        /// </returns>
        public override async Task<IEnumerable<string>> GetAllAsync(CancellationToken cancellationToken)
        {
            ActivityLoggingEventSource.Log.MethodEnter(
                "GetAllAsync(CancellationToken cancellationToken)",
                LoggingSourceName,
                this.logger);

            var effectiveTldNames = LocalResourceData.EffectiveTldNames;

            using (var sr = new StringReader(effectiveTldNames))
            {
                var data = new List<string>();

                string line;
                while ((line = await sr.ReadLineAsync().ConfigureAwait(false)) != null)
                {
                    data.Add(line);
                }

                ActivityLoggingEventSource.Log.MethodExit(
                    "GetAllAsync(CancellationToken cancellationToken)",
                    LoggingSourceName,
                    this.logger);

                return data;
            }
        }
    }
}