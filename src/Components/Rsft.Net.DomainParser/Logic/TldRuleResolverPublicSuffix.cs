﻿// <copyright file="TldRuleResolverPublicSuffix.cs" company="Rolosoft Ltd">
// Copyright (c) Rolosoft Ltd. All rights reserved.
// </copyright>

namespace Rsft.Net.DomainParser.Logic
{
    using System;
    using Rsft.Net.DomainParser.Core.Entities.Logic;
    using Rsft.Net.DomainParser.Core.Interfaces;

    /// <summary>
    /// The TLD rule resolver.
    /// </summary>
    internal sealed class TldRuleResolverPublicSuffix : ITldRuleResolver<TldRule>
    {
        /// <summary>
        /// The resolve.
        /// </summary>
        /// <param name="ruleInfo">
        /// The rule info.
        /// </param>
        /// <remarks>
        /// <para>Construct a TLDRule based on a single line from the www.publicsuffix.org list.</para>
        /// </remarks>
        /// <returns>
        /// The <see cref="TldRule"/>.
        /// </returns>
        public TldRule Resolve(string ruleInfo)
        {
            var rtn = new TldRule();

            // Parse the rule and set properties accordingly:
            if (ruleInfo.StartsWith("*", StringComparison.OrdinalIgnoreCase))
            {
                rtn.RuleType = RuleType.Wildcard;
                rtn.Name = ruleInfo.Substring(2);
            }
            else if (ruleInfo.StartsWith("!", StringComparison.OrdinalIgnoreCase))
            {
                rtn.RuleType = RuleType.Exception;
                rtn.Name = ruleInfo.Substring(1);
            }
            else
            {
                rtn.RuleType = RuleType.Normal;
                rtn.Name = ruleInfo;
            }

            return rtn;
        }
    }
}