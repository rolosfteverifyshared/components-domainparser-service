﻿// <copyright file="TldRulesCache.cs" company="Rolosoft Ltd">
// Copyright (c) Rolosoft Ltd. All rights reserved.
// </copyright>

namespace Rsft.Net.DomainParser.Logic
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Threading;
    using System.Threading.Tasks;
    using JetBrains.Annotations;
    using Microsoft.Extensions.Caching.Memory;
    using Microsoft.Extensions.Logging;
    using Rsft.Net.DomainParser.Core.Entities.Logic;
    using Rsft.Net.DomainParser.Core.Entities.Logic.Configuration;
    using Rsft.Net.DomainParser.Core.Interfaces;
    using Rsft.Net.DomainParser.Diagnostics.EventSources;
    using Rsft.Net.DomainParser.Helpers;

    /// <summary>
    ///     The TLD rules cache.
    /// </summary>
    internal sealed class TldRulesCache : ITldRulesCache
    {
        /// <summary>
        ///     The logging module name.
        /// </summary>
        private const string LoggingSourceName = @"Rsft.Net.DomainParser.Logic.TLDRulesCache";

        /// <summary>
        ///     The cache key.
        /// </summary>
        private const string CacheKey1 = @"66EF5BF8-6BE2-4157-8A98-4E783DB60013";

        /// <summary>
        /// My cache.
        /// </summary>
        private static readonly MemoryCache MyCache = new MemoryCache(new MemoryCacheOptions());

        /// <summary>
        ///     The external data repo.
        /// </summary>
        [NotNull]
        private readonly IReadOnlyRepository<string, string> externalDataRepo;

        /// <summary>
        ///     The local backup repo.
        /// </summary>
        [NotNull]
        private readonly IReadOnlyRepository<string, string> localBackupRepo;

        /// <summary>
        ///     The configuration.
        /// </summary>
        [NotNull]
        private readonly IConfiguration<DomainParserConfiguration> myConfiguration;

        /// <summary>
        ///     The rule resolver.
        /// </summary>
        [NotNull]
        private readonly ITldRuleResolver<TldRule> ruleResolver;

        /// <summary>
        /// The logger.
        /// </summary>
        [NotNull]
        private readonly ILogger logger;

        /// <summary>
        /// Initializes a new instance of the <see cref="TldRulesCache" /> class.
        /// </summary>
        /// <param name="configuration">The configuration.</param>
        /// <param name="ruleResolver">The rule resolver.</param>
        /// <param name="localBackupRepo">The local backup repo.</param>
        /// <param name="externalDataRepo">The external data repo.</param>
        /// <param name="loggerFactory">The logger factory.</param>
        public TldRulesCache(
            [NotNull] IConfiguration<DomainParserConfiguration> configuration,
            [NotNull] ITldRuleResolver<TldRule> ruleResolver,
            [NotNull] IReadOnlyRepository<string, string> localBackupRepo,
            [NotNull] IReadOnlyRepository<string, string> externalDataRepo,
            [NotNull] ILoggerFactory loggerFactory)
        {
            this.myConfiguration = configuration;
            this.ruleResolver = ruleResolver;
            this.localBackupRepo = localBackupRepo;
            this.externalDataRepo = externalDataRepo;
            this.logger = loggerFactory.CreateLogger<TldRulesCache>();
        }

        /// <summary>
        ///     Gets the TLD rule lists.
        /// </summary>
        /// <value>
        ///     The TLD rule lists.
        /// </value>
        public IDictionary<RuleType, IDictionary<string, TldRule>> TldRuleLists => this.ReadTldRules();

        /// <summary>
        /// Gets the or add existing.
        /// </summary>
        /// <param name="key">The key.</param>
        /// <returns>A <see cref="Task"/> representing the asynchronous operation.</returns>
        private async Task<IDictionary<RuleType, IDictionary<string, TldRule>>> GetOrAddExisting([NotNull] string key)
        {
            var value = await MyCache.GetOrCreateAsync(
                key,
                async entry =>
                {
                    if (entry.Value != null)
                    {
                        var isoCountries = (IDictionary<RuleType, IDictionary<string, TldRule>>)entry.Value;

                        return isoCountries;
                    }

                    var tldRulesAsync = await this.GetTldRulesAsync(CancellationToken.None).ConfigureAwait(false);

                    entry.AbsoluteExpiration = DateTimeOffset.UtcNow.AddDays(1);

                    entry.Value = tldRulesAsync;

                    return tldRulesAsync;
                }).ConfigureAwait(false);

            return value;
        }

        /// <summary>
        /// Reads the rules asynchronously using local repo as backup.
        /// </summary>
        /// <param name="cancellationToken">
        /// The cancellation token.
        /// </param>
        /// <returns>
        /// The <see cref="Task"/>.
        /// </returns>
        private async Task<List<string>> ReadRulesWithRedundancyAsync(CancellationToken cancellationToken)
        {
            ActivityLoggingEventSource.Log.MethodEnter(
                "ReadPublicRulesAsync(CancellationToken cancellationToken)",
                LoggingSourceName,
                this.logger);

            var useSecondary = false;

            IEnumerable<string> list = null;

            try
            {
                /*Try remote data source first*/
                list = await this.externalDataRepo.GetAllAsync(cancellationToken).ConfigureAwait(false);
            }
            catch (AggregateException aggregateException)
            {
                aggregateException.Handle(
                    ae =>
                        {
                            ExceptionLoggingEventSource.Log.WarningRemoteResourceUnavailable(
                                this.myConfiguration.Configuration.TldPublicRulesUrl,
                                ae.Message);
                            useSecondary = true;
                            return true;
                        });
            }
            catch (Exception exception)
            {
                ExceptionLoggingEventSource.Log.WarningRemoteResourceUnavailable(
                                this.myConfiguration.Configuration.TldPublicRulesUrl,
                                exception.Message);
                useSecondary = true;
            }

            if (!useSecondary)
            {
                ActivityLoggingEventSource.Log.MethodExit(
                    "ReadPublicRulesAsync(CancellationToken cancellationToken)",
                    LoggingSourceName,
                    this.logger);

                return list.ToSafeEnumerable().ToList();
            }

            try
            {
                /*If remote source is FUBAR, use local copy*/
                list = await this.localBackupRepo.GetAllAsync(CancellationToken.None).ConfigureAwait(false);
            }
            catch (AggregateException aggregateException)
            {
                aggregateException.Handle(
                    ae =>
                        {
                            ExceptionLoggingEventSource.Log.Error(ae);
                            return false;
                        });
            }

            ActivityLoggingEventSource.Log.MethodExit(
                "ReadPublicRulesAsync(CancellationToken cancellationToken)",
                LoggingSourceName,
                this.logger);

            return list.ToSafeEnumerable().ToList();
        }

        /// <summary>
        /// Gets the list of TLD rules from the cache.
        /// </summary>
        /// <param name="cancellationToken">
        /// The cancellation Token.
        /// </param>
        /// <returns>
        /// The IDictionary.
        /// </returns>
        private async Task<IDictionary<RuleType, IDictionary<string, TldRule>>> GetTldRulesAsync(CancellationToken cancellationToken)
        {
            ActivityLoggingEventSource.Log.MethodEnter(
                "GetTldRulesAsync(CancellationToken cancellationToken)",
                LoggingSourceName,
                this.logger);

            var results = new Dictionary<RuleType, IDictionary<string, TldRule>>();
            var rules = Enum.GetValues(typeof(RuleType)).Cast<RuleType>();
            foreach (var rule in rules)
            {
                results[rule] = new Dictionary<string, TldRule>(StringComparer.OrdinalIgnoreCase);
            }

            var ruleStrings = await this.ReadRulesWithRedundancyAsync(cancellationToken).ConfigureAwait(false);

            // Strip out any lines that are a comment or blank
            foreach (var result in
                ruleStrings.Where(
                    ruleString =>
                    !ruleString.StartsWith("//", StringComparison.OrdinalIgnoreCase)
                    && ruleString.Trim().Length != 0).Select(ruleString => this.ruleResolver.Resolve(ruleString)))
            {
                results[result.RuleType][result.Name] = result;
            }

            ActivityLoggingEventSource.Log.TldRulesLoadedIntoCache(results.Values.Sum(r => r.Values.Count), this.logger);

            ActivityLoggingEventSource.Log.MethodExit(
                "GetTldRulesAsync(CancellationToken cancellationToken)",
                LoggingSourceName,
                this.logger);

            return results;
        }

        /// <summary>
        /// Reads the TLD rules.
        /// </summary>
        /// <returns>
        /// The IDictionary.
        /// </returns>
        private IDictionary<RuleType, IDictionary<string, TldRule>> ReadTldRules()
        {
            var item = this.GetOrAddExisting(CacheKey1).Result;

            return item;
        }
    }
}