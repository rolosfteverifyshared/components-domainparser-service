﻿// <copyright file="DefaultConfiguration.cs" company="Rolosoft Ltd">
// Copyright (c) Rolosoft Ltd. All rights reserved.
// </copyright>

// Copyright 2019 Rolosoft Ltd
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
// http://www.apache.org/licenses/LICENSE-2.0
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
namespace Rsft.Net.DomainParser.Logic.Configuration
{
    using System;
    using Rsft.Net.DomainParser.Core.Entities.Logic.Configuration;
    using Rsft.Net.DomainParser.Core.Interfaces;

    /// <summary>
    /// The default configuration.
    /// </summary>
    internal sealed class DefaultConfiguration : IConfiguration<DomainParserConfiguration>
    {
        /// <summary>
        ///     The public rules.
        /// </summary>
        private const string DefaultPublicRules = @"https://publicsuffix.org/list/effective_tld_names.dat";

        /// <summary>
        ///     The default TLD public fetch timeout.
        /// </summary>
        private const int DefaultTldPublicFetchTimeout = 8000;

        /// <summary>
        /// Gets or sets the configuration.
        /// </summary>
        /// <exception cref="NotImplementedException">
        /// Set is not implemented.
        /// </exception>
        public DomainParserConfiguration Configuration
        {
            get => new DomainParserConfiguration
            {
                TldPublicFetchTimeout = DefaultTldPublicFetchTimeout,
                TldPublicRulesUrl = DefaultPublicRules,
            };

            set => throw new NotImplementedException();
        }
    }
}