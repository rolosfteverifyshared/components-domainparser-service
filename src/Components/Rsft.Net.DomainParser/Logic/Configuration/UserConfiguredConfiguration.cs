﻿// <copyright file="UserConfiguredConfiguration.cs" company="Rolosoft Ltd">
// Copyright (c) Rolosoft Ltd. All rights reserved.
// </copyright>

// Copyright 2019 Rolosoft Ltd
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
// http://www.apache.org/licenses/LICENSE-2.0
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
namespace Rsft.Net.DomainParser.Logic.Configuration
{
    using Rsft.Net.DomainParser.Core.Entities.Logic.Configuration;
    using Rsft.Net.DomainParser.Core.Interfaces;

    /// <summary>
    /// The user configured configuration.
    /// </summary>
    internal sealed class UserConfiguredConfiguration : IConfiguration<DomainParserConfiguration>
    {
        /// <summary>
        /// Gets or sets the configuration.
        /// </summary>
        public DomainParserConfiguration Configuration { get; set; }
    }
}