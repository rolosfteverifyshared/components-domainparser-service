﻿// <copyright file="DomainParser.cs" company="Rolosoft Ltd">
// Copyright (c) Rolosoft Ltd. All rights reserved.
// </copyright>

namespace Rsft.Net.DomainParser.Logic
{
    using System;
    using System.Collections.Generic;
    using System.Diagnostics;
    using System.Diagnostics.Contracts;
    using System.Linq;

    using JetBrains.Annotations;
    using Microsoft.Extensions.Logging;
    using Rsft.Net.DomainParser.Core.Entities.Logic;
    using Rsft.Net.DomainParser.Core.Interfaces;

    /// <summary>
    /// The domain parser.
    /// </summary>
    internal sealed class DomainParser : IDomainParser<DomainInfo>
    {
        /// <summary>
        ///     The logging module name.
        /// </summary>
        private const string LoggingSourceName = @"Rsft.Net.DomainParser.Logic.DomainParser";

        /// <summary>
        /// The rules cache.
        /// </summary>
        [NotNull]
        private readonly ITldRulesCache rulesCache;

        /// <summary>
        /// The logger.
        /// </summary>
        [NotNull]
        private readonly ILogger logger;

        /// <summary>
        /// Initializes a new instance of the <see cref="DomainParser" /> class.
        /// </summary>
        /// <param name="rulesCache">The rules cache.</param>
        /// <param name="loggerFactory">The logger factory.</param>
        public DomainParser([NotNull] ITldRulesCache rulesCache, [NotNull] ILoggerFactory loggerFactory)
        {
            this.rulesCache = rulesCache;
            this.logger = loggerFactory.CreateLogger<DomainParser>();
        }

        /// <summary>
        /// The parse.
        /// </summary>
        /// <param name="domainString">
        /// The domain string.
        /// </param>
        /// <returns>
        /// The <see cref="DomainInfo"/>.
        /// </returns>
        public DomainInfo Parse(string domainString)
        {
            Diagnostics.EventSources.ActivityLoggingEventSource.Log.MethodEnter("Parse(string domainString)", LoggingSourceName, this.logger);

            var rtn = new DomainInfo();

            var findMatchingTldRule = this.FindMatchingTldRule(domainString);

            if (findMatchingTldRule == null)
            {
                throw new FormatException("The domain does not have a recognized TLD");
            }

            rtn.MatchingRule = findMatchingTldRule;

            // Based on the tld rule found, get the domain (and possibly the subdomain)
            string tempSudomainAndDomain;
            var tldIndex = 0;

            // First, determine what type of rule we have, and set the TLD accordingly
            switch (findMatchingTldRule.RuleType)
            {
                case RuleType.Normal:
                    tldIndex = domainString.LastIndexOf(
                        "." + findMatchingTldRule.Name,
                        StringComparison.OrdinalIgnoreCase);
                    tempSudomainAndDomain = domainString.Substring(0, tldIndex);
                    rtn.Tld = domainString.Substring(tldIndex + 1);
                    break;
                case RuleType.Wildcard:

                    // This finds the last portion of the TLD...
                    tldIndex = domainString.LastIndexOf(
                        "." + findMatchingTldRule.Name,
                        StringComparison.OrdinalIgnoreCase);
                    tempSudomainAndDomain = domainString.Substring(0, tldIndex);

                    // But we need to find the wildcard portion of it:
                    tldIndex = tempSudomainAndDomain.LastIndexOf(".", StringComparison.OrdinalIgnoreCase);
                    tempSudomainAndDomain = domainString.Substring(0, tldIndex);
                    rtn.Tld = domainString.Substring(tldIndex + 1);
                    break;
                case RuleType.Exception:
                    tldIndex = domainString.LastIndexOf(".", StringComparison.OrdinalIgnoreCase);
                    tempSudomainAndDomain = domainString.Substring(0, tldIndex);
                    rtn.Tld = domainString.Substring(tldIndex + 1);
                    break;
                default:
                    throw new ArgumentOutOfRangeException();
            }

            // See if we have a subdomain:
            var lstRemainingParts = new List<string>(tempSudomainAndDomain.Split('.'));

            if (lstRemainingParts.Count <= 0)
            {
                Diagnostics.EventSources.ActivityLoggingEventSource.Log.MethodExit("Parse(string domainString)", LoggingSourceName, this.logger);
                return rtn;
            }

            /*
            If we have 0 parts left, there is just a tld and no domain or subdomain
            If we have 1 part, it's the domain, and there is no subdomain
            If we have 2+ parts, the last part is the domain, the other parts (combined) are the subdomain
            */

            // Set the domain:
            rtn.Sld = lstRemainingParts[lstRemainingParts.Count - 1];

            // Set the subdomain, if there is one to set:
            if (lstRemainingParts.Count > 1)
            {
                // We strip off the trailing period, too
                rtn.Subdomain = tempSudomainAndDomain.Substring(0, tempSudomainAndDomain.Length - rtn.Sld.Length - 1);
            }

            Diagnostics.EventSources.ActivityLoggingEventSource.Log.MethodExit("Parse(string domainString)", LoggingSourceName, this.logger);

            return rtn;
        }

        /// <summary>
        /// The try parse.
        /// </summary>
        /// <param name="domainString">
        /// The domain string.
        /// </param>
        /// <param name="response">
        /// The response.
        /// </param>
        /// <returns>
        /// The <see cref="bool"/>.
        /// </returns>
        public bool TryParse(string domainString, out DomainInfo response)
        {
            var rtn = false;
            response = null;

            try
            {
                var domainInfo = this.Parse(domainString);

                response = domainInfo;

                rtn = true;
            }
            catch
            {
                // ignored
            }

            return rtn;
        }

        /// <summary>
        /// Finds matching rule for a domain. If no rule is found, returns a null TLD rule object.
        /// </summary>
        /// <param name="domainString">
        /// The domain string.
        /// </param>
        /// <returns>
        /// The <see cref="TldRule"/>.
        /// </returns>
        private TldRule FindMatchingTldRule(string domainString)
        {
            Contract.Requires(!string.IsNullOrWhiteSpace(domainString));

            Diagnostics.EventSources.ActivityLoggingEventSource.Log.MethodEnter("FindMatchingTldRule(string domainString)", LoggingSourceName, this.logger);

            // Split our domain into parts (based on the '.')
            // ...Put these parts in a list
            // ...Make sure these parts are in reverse order (we'll be checking rules from the right-most pat of the domain)
            var lstDomainParts = domainString.Split('.').ToList();
            lstDomainParts.Reverse();

            // Begin building our partial domain to check rules with:
            var checkAgainst = string.Empty;

            // Our 'matches' collection:
            var ruleMatches = new List<TldRule>();

            foreach (var domainPart in lstDomainParts)
            {
                // Add on our next domain part:
                checkAgainst = string.Format("{0}.{1}", domainPart, checkAgainst);

                // If we end in a period, strip it off:
                if (checkAgainst.EndsWith("."))
                {
                    checkAgainst = checkAgainst.Substring(0, checkAgainst.Length - 1);
                }

                var rules = Enum.GetValues(typeof(RuleType)).Cast<RuleType>();
                foreach (var rule in rules)
                {
                    // Try to match rule:
                    TldRule result;
                    if (this.rulesCache.TldRuleLists[rule].TryGetValue(checkAgainst, out result))
                    {
                        ruleMatches.Add(result);
                    }

#if DEBUG
                    Debug.WriteLine("Domain part {0} matched {1} {2} rules", checkAgainst, result == null ? 0 : 1, rule);
#endif
                }
            }

            // Sort our matches list (longest rule wins, according to :
            var results = from match in ruleMatches orderby match.Name.Length descending select match;

            // Take the top result (our primary match):
            var primaryMatch = results.Take(1).SingleOrDefault();

#if DEBUG
            if (primaryMatch != null)
            {
                Debug.WriteLine(
                    "Looks like our match is: {0}, which is a(n) {1} rule.",
                    primaryMatch.Name,
                    primaryMatch.RuleType);
            }
            else
            {
                Debug.WriteLine(string.Format("No rules matched domain: {0}", domainString));
            }

#endif
            Diagnostics.EventSources.ActivityLoggingEventSource.Log.MethodExit("FindMatchingTldRule(string domainString)", LoggingSourceName, this.logger);

            return primaryMatch;
        }
    }
}