﻿// <copyright file="LocalResourceData.cs" company="Rolosoft Ltd">
// Copyright (c) Rolosoft Ltd. All rights reserved.
// </copyright>

namespace Rsft.Net.DomainParser.Logic.Data
{
    using System.Diagnostics;
    using System.Diagnostics.Contracts;
    using System.Globalization;
    using System.IO;
    using System.Reflection;

    using Rsft.Net.DomainParser.Data;

    /// <summary>
    ///     The local resource data.
    /// </summary>
    internal static class LocalResourceData
    {
        /// <summary>
        /// My type information.
        /// </summary>
        private static readonly TypeInfo MyTypeInfo = typeof(DomainParserFactory).GetTypeInfo();

        /// <summary>
        ///     The executing assembly name.
        /// </summary>
        private static readonly string ExecutingAssemblyName = MyTypeInfo.Namespace;

        /// <summary>
        ///     Gets the effective TLD names.
        /// </summary>
        /// <value>
        ///     The effective TLD names.
        /// </value>
        public static string EffectiveTldNames
        {
            get
            {
                Contract.Ensures(Contract.Result<string>() != null);

                var filePath = string.Concat(ExecutingAssemblyName, ".", DataIndex.EffectiveTldNames);

                return GetLocalResourceFile(filePath);
            }
        }

        /// <summary>
        /// Gets the local resource file.
        /// </summary>
        /// <param name="filePath">
        /// The file path.
        /// </param>
        /// <returns>
        /// The <see cref="string"/>.
        /// </returns>
        private static string GetLocalResourceFile(string filePath)
        {
            Contract.Requires(filePath != null);

            var typeInfo = typeof(DomainParserFactory).GetTypeInfo();

            var assembly = typeInfo.Assembly;

            using (var s = assembly.GetManifestResourceStream(filePath))
            {
                Debug.Assert(
                    s != null && s.Length > 0,
                    string.Format(CultureInfo.InvariantCulture, "file '{0}' cannot be loaded", filePath));

                using (var sr = new StreamReader(s))
                {
                    var readToEnd = sr.ReadToEnd();

                    return readToEnd;
                }
            }
        }
    }
}