﻿// <copyright file="DomainParserFactoryTests.cs" company="Rolosoft Ltd">
// (c) 2017  Rolosoft Ltd
// </copyright>

// Copyright 2016 Rolosoft Ltd
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
// http://www.apache.org/licenses/LICENSE-2.0
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
namespace Rsft.Net.DomainParser.Tests.Integration
{
    using System;
    using System.Collections.Generic;
    using System.Threading;
    using System.Threading.Tasks;
    using JetBrains.Annotations;
    using Moq;
    using Rsft.Net.DomainParser.Core.Entities.Service;
    using Rsft.Net.DomainParser.Core.Interfaces;
    using Xunit;
    using Xunit.Abstractions;

#if DEBUG

    /// <summary>
    /// The domain parser factory tests.
    /// </summary>
    public class DomainParserFactoryTests : TestBase
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="DomainParserFactoryTests"/> class.
        /// </summary>
        /// <param name="outputHelper">The output helper.</param>
        public DomainParserFactoryTests([NotNull] ITestOutputHelper outputHelper)
            : base(outputHelper)
        {
        }

        /// <summary>
        /// Factories the default tests.
        /// </summary>
        /// <param name="domainString">The domain string.</param>
        /// <param name="expected">The expected.</param>
        [Theory]
        [MemberData(nameof(MyTestCases.Cases), MemberType = typeof(MyTestCases))]
        public void FactoryDefault_Tests(string domainString, Tuple<string, string, string> expected)
        {
            // arrange
            var domainParser = DomainParserFactory.Create();

            // act
            var domainInfo = domainParser.Parse(domainString);

            // assert
            this.OutputHelper.WriteLine("TLD: {0}", domainInfo.Tld);
            this.OutputHelper.WriteLine("Second Level Domain: {0}", domainInfo.Sld);
            this.OutputHelper.WriteLine("Sub domain: {0}", domainInfo.Subdomain);

            Assert.Equal(new Tuple<string, string, string>(domainInfo.Tld, domainInfo.Sld, domainInfo.Subdomain), expected);
        }

        /// <summary>
        /// Factories the custom configuration tests.
        /// </summary>
        /// <param name="domainString">The domain string.</param>
        /// <param name="expected">The expected.</param>
        [Theory]
        [MemberData(nameof(MyTestCases.Cases), MemberType = typeof(MyTestCases))]
        public void FactoryCustomConfig_Tests(string domainString, Tuple<string, string, string> expected)
        {
            // arrange
            var mockConfiguration = new Mock<IConfiguration<Configuration>>();

            mockConfiguration.Setup(r => r.Configuration)
                .Returns(() => new Configuration { TldPublicFetchTimeout = 10 });

            var domainParser = DomainParserFactory.Create(mockConfiguration.Object);

            // act
            var domainInfo = domainParser.Parse(domainString);

            // assert
            this.OutputHelper.WriteLine("TLD: {0}", domainInfo.Tld);
            this.OutputHelper.WriteLine("Second Level Domain: {0}", domainInfo.Sld);
            this.OutputHelper.WriteLine("Sub domain: {0}", domainInfo.Subdomain);

            mockConfiguration.Verify(r => r.Configuration, Times.Exactly(3));
            Assert.Equal(new Tuple<string, string, string>(domainInfo.Tld, domainInfo.Sld, domainInfo.Subdomain), expected);
        }

        /// <summary>
        /// Factories the custom repo tests.
        /// </summary>
        /// <param name="domainString">The domain string.</param>
        /// <param name="expected">The expected.</param>
        [Theory]
        [MemberData(nameof(MyTestCases.Cases), MemberType = typeof(MyTestCases))]
        public void FactoryCustomRepo_Tests(string domainString, Tuple<string, string, string> expected)
        {
            // arrange
            var mockRepo = new Mock<IReadOnlyRepository<string, string>>();
            mockRepo.Setup(r => r.GetAllAsync(It.IsAny<CancellationToken>()))
                .Returns(
                    () => Task.FromResult<IEnumerable<string>>(new List<string> { "com", "com.br", "co", "io", "uk", "co.uk" }));

            var domainParser = DomainParserFactory.Create(null, mockRepo.Object);

            // act
            var domainInfo = domainParser.Parse(domainString);

            // assert
            Assert.Equal(new Tuple<string, string, string>(domainInfo.Tld, domainInfo.Sld, domainInfo.Subdomain), expected);
        }

        private static class MyTestCases
        {
            public static IEnumerable<object[]> Cases => new List<object[]>
            {
                new object[] { "me.here.com", new Tuple<string, string, string>("com", "here", "me") },
                new object[] { "me.here.com.br", new Tuple<string, string, string>("com.br", "here", "me") },
                new object[] { "me.here.co", new Tuple<string, string, string>("co", "here", "me") },
                new object[] { "me.here.io", new Tuple<string, string, string>("io", "here", "me") },
                new object[] { "me.here.uk", new Tuple<string, string, string>("uk", "here", "me") },
                new object[] { "me.here.co.uk", new Tuple<string, string, string>("co.uk", "here", "me") },
                new object[] { "here.uk", new Tuple<string, string, string>("uk", "here", null) },
            };

            /*public static IEnumerable Cases
            {
                get
                {
                    yield return new TestCaseData("me.here.com").Returns(new Tuple<string, string, string>("com", "here", "me"));
                    yield return new TestCaseData("me.here.com.br").Returns(new Tuple<string, string, string>("com.br", "here", "me"));
                    yield return new TestCaseData("me.here.co").Returns(new Tuple<string, string, string>("co", "here", "me"));
                    yield return new TestCaseData("me.here.io").Returns(new Tuple<string, string, string>("io", "here", "me"));
                    yield return new TestCaseData("me.here.uk").Returns(new Tuple<string, string, string>("uk", "here", "me"));
                    yield return new TestCaseData("me.here.co.uk").Returns(new Tuple<string, string, string>("co.uk", "here", "me"));
                    yield return new TestCaseData("here.uk").Returns(new Tuple<string, string, string>("uk", "here", null));
                }
            }*/
        }
    }
#endif

}