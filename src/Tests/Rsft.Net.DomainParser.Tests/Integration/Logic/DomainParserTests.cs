﻿// <copyright file="DomainParserTests.cs" company="Rolosoft Ltd">
// (c) 2017  Rolosoft Ltd
// </copyright>

// Copyright 2017 Rolosoft Ltd
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
// http://www.apache.org/licenses/LICENSE-2.0
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
namespace Rsft.Net.DomainParser.Tests.Integration.Logic
{
    using System;
    using System.Collections.Generic;
    using System.Diagnostics;
    using JetBrains.Annotations;
    using Rsft.Net.DomainParser.Logic;
    using Rsft.Net.DomainParser.Logic.Configuration;
    using Rsft.Net.DomainParser.Logic.Repository;
    using Xunit;
    using Xunit.Abstractions;

#if DEBUG
    /// <summary>
    /// The domain parser tests.
    /// </summary>
    public class DomainParserTests : TestBase
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="DomainParserTests"/> class.
        /// </summary>
        /// <param name="outputHelper">The output helper.</param>
        public DomainParserTests([NotNull] ITestOutputHelper outputHelper)
            : base(outputHelper)
        {
        }

        /// <summary>
        /// Parses the test cases.
        /// </summary>
        /// <param name="domainString">The domain string.</param>
        /// <param name="expected">The expected.</param>
        [Theory]
        [MemberData(nameof(MyTestCases.Cases), MemberType = typeof(MyTestCases))]
        public void Parse_TestCases(string domainString, Tuple<string, string, string> expected)
        {
            // arrange
            var defaultConfiguration = new DefaultConfiguration();

            var domainParser = new DomainParser(
                new TldRulesCache(
                    defaultConfiguration,
                    new TldRuleResolverPublicSuffix(),
                    new LocalResourceRepository(this.LoggerFactory),
                    new PublicSuffixRepository(defaultConfiguration, this.LoggerFactory),
                    this.LoggerFactory),
                this.LoggerFactory);

            // act
            var stopwatch = Stopwatch.StartNew();
            var domainInfo = domainParser.Parse(domainString);
            stopwatch.Stop();

            // assert
            this.WriteTimeElapsed(stopwatch.ElapsedMilliseconds);
            Assert.Equal(new Tuple<string, string, string>(domainInfo.Tld, domainInfo.Sld, domainInfo.Subdomain), expected);
        }

        /// <summary>
        /// Test cases.
        /// </summary>
        private static class MyTestCases
        {
            /// <summary>
            /// Gets the cases.
            /// </summary>
            /// <value>
            /// The cases.
            /// </value>
            public static IEnumerable<object[]> Cases => new List<object[]>
            {
                new object[] { "me.here.com", new Tuple<string, string, string>("com", "here", "me") },
                new object[] { "me.here.com.br", new Tuple<string, string, string>("com.br", "here", "me") },
                new object[] { "me.here.co", new Tuple<string, string, string>("co", "here", "me") },
                new object[] { "me.here.io", new Tuple<string, string, string>("io", "here", "me") },
                new object[] { "me.here.uk", new Tuple<string, string, string>("uk", "here", "me") },
                new object[] { "me.here.co.uk", new Tuple<string, string, string>("co.uk", "here", "me") },
                new object[] { "here.uk", new Tuple<string, string, string>("uk", "here", null) },
            };

            /*public static IEnumerable Cases
            {
                get
                {
                    yield return new TestCaseData("me.here.com").Returns(new Tuple<string, string, string>("com", "here", "me"));
                    yield return new TestCaseData("me.here.com.br").Returns(new Tuple<string, string, string>("com.br", "here", "me"));
                    yield return new TestCaseData("me.here.co").Returns(new Tuple<string, string, string>("co", "here", "me"));
                    yield return new TestCaseData("me.here.io").Returns(new Tuple<string, string, string>("io", "here", "me"));
                    yield return new TestCaseData("me.here.uk").Returns(new Tuple<string, string, string>("uk", "here", "me"));
                    yield return new TestCaseData("me.here.co.uk").Returns(new Tuple<string, string, string>("co.uk", "here", "me"));
                    yield return new TestCaseData("here.uk").Returns(new Tuple<string, string, string>("uk", "here", null));
                }
            }*/
        }
    }
#endif
}