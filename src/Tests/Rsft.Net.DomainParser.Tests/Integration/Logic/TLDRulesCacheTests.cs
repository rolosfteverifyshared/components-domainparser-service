﻿// <copyright file="TLDRulesCacheTests.cs" company="Rolosoft Ltd">
// (c) 2017  Rolosoft Ltd
// </copyright>

// Copyright 2017 Rolosoft Ltd
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
// http://www.apache.org/licenses/LICENSE-2.0
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
namespace Rsft.Net.DomainParser.Tests.Integration.Logic
{
    using System.Diagnostics;
    using JetBrains.Annotations;
    using Rsft.Net.DomainParser.Logic;
    using Rsft.Net.DomainParser.Logic.Configuration;
    using Rsft.Net.DomainParser.Logic.Repository;
    using Xunit;
    using Xunit.Abstractions;

#if DEBUG

    /// <summary>
    /// The TLD rules cache tests.
    /// </summary>
    public class TldRulesCacheTests : TestBase
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="TldRulesCacheTests"/> class.
        /// </summary>
        /// <param name="outputHelper">The output helper.</param>
        public TldRulesCacheTests([NotNull] ITestOutputHelper outputHelper)
            : base(outputHelper)
        {
        }

        /// <summary>
        /// The instance_ when repeat_ expect caching.
        /// </summary>
        [Fact]
        public void Instance_WhenRepeat_ExpectCaching()
        {
            // arrange
            var tldRulesCache = new TldRulesCache(
                new DefaultConfiguration(),
                new TldRuleResolverPublicSuffix(),
                new LocalResourceRepository(this.LoggerFactory),
                new PublicSuffixRepository(new DefaultConfiguration(), this.LoggerFactory),
                this.LoggerFactory);

            // act
            for (int i = 0; i < 5; i++)
            {
                var stopWatch = Stopwatch.StartNew();
                var tldRuleLists = tldRulesCache.TldRuleLists;
                stopWatch.Stop();

                // assert
                Assert.True(tldRuleLists.Count > 0);
                this.WriteTimeElapsed(stopWatch.ElapsedMilliseconds);
            }
        }
    }

#endif
}