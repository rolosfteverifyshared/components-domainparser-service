﻿// <copyright file="PublicSuffixRepositoryTests.cs" company="Rolosoft Ltd">
// (c) 2017  Rolosoft Ltd
// </copyright>

// Copyright 2017 Rolosoft Ltd
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
// http://www.apache.org/licenses/LICENSE-2.0
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
namespace Rsft.Net.DomainParser.Tests.Unit.Repository
{
    using System;
    using System.Diagnostics;
    using System.Linq;
    using JetBrains.Annotations;
    using Moq;
    using Rsft.Net.DomainParser.Core.Entities.Logic.Configuration;
    using Rsft.Net.DomainParser.Core.Interfaces;
    using Rsft.Net.DomainParser.Logic.Repository;
    using Xunit;
    using Xunit.Abstractions;

#if DEBUG

    /// <summary>
    /// The public suffix repository tests.
    /// </summary>
    public class PublicSuffixRepositoryTests : TestBase
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="PublicSuffixRepositoryTests"/> class.
        /// </summary>
        /// <param name="outputHelper">The output helper.</param>
        public PublicSuffixRepositoryTests([NotNull] ITestOutputHelper outputHelper)
            : base(outputHelper)
        {
        }

        /// <summary>
        /// The get all_ when reading public suffix remote_ expect read remote.
        /// </summary>
        [Fact]
        public void GetAll_WhenReadingPublicSuffixRemote_ExpectReadRemote()
        {
            // arrange
            var mockConfiguration = new Mock<IConfiguration<DomainParserConfiguration>>();

            mockConfiguration.Setup(r => r.Configuration).Returns(() => new DomainParserConfiguration { TldPublicFetchTimeout = int.MaxValue, TldPublicRulesUrl = @"https://publicsuffix.org/list/effective_tld_names.dat" });

            var publicSuffixRepository = new PublicSuffixRepository(mockConfiguration.Object, this.LoggerFactory);

            // act
            var stopwatch = Stopwatch.StartNew();
            var enumerable = publicSuffixRepository.GetAll();
            stopwatch.Stop();

            // assert
            mockConfiguration.Verify(r => r.Configuration, Times.AtLeastOnce);
            Assert.True(enumerable.Any());
            this.WriteTimeElapsed(stopwatch.ElapsedMilliseconds);
        }

        /// <summary>
        /// Gets all when reading public suffix remote with short timeout expect timeout exception.
        /// </summary>
        [Fact]
        public void GetAll_WhenReadingPublicSuffixRemoteWithShortTimeout_ExpectTimeoutException()
        {
            // arrange
            var mockConfiguration = new Mock<IConfiguration<DomainParserConfiguration>>();

            mockConfiguration.Setup(r => r.Configuration).Returns(() => new DomainParserConfiguration { TldPublicFetchTimeout = 1, TldPublicRulesUrl = @"https://publicsuffix.org/list/effective_tld_names.dat" });

            var publicSuffixRepository = new PublicSuffixRepository(mockConfiguration.Object, this.LoggerFactory);

            var testPass = false;

            var stopwatch = Stopwatch.StartNew();

            // act
            try
            {
                publicSuffixRepository.GetAll();
            }
            catch (AggregateException aggregateException)
            {
                aggregateException.Handle(
                    ae =>
                        {
                            if (ae.GetType() == typeof(TimeoutException))
                            {
                                testPass = true;
                            }

                            return true;
                        });
            }

            stopwatch.Stop();

            // assert
            mockConfiguration.Verify(r => r.Configuration, Times.AtLeastOnce);
            this.WriteTimeElapsed(stopwatch.ElapsedMilliseconds);
            Assert.True(testPass);
        }
    }

#endif

}