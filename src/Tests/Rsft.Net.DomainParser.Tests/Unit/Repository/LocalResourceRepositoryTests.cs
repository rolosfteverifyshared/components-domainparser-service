﻿// <copyright file="LocalResourceRepositoryTests.cs" company="Rolosoft Ltd">
// (c) 2017  Rolosoft Ltd
// </copyright>

// Copyright 2017 Rolosoft Ltd
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
// http://www.apache.org/licenses/LICENSE-2.0
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
namespace Rsft.Net.DomainParser.Tests.Unit.Repository
{
    using System.Diagnostics;
    using System.Linq;
    using JetBrains.Annotations;
    using Rsft.Net.DomainParser.Logic.Repository;
    using Xunit;
    using Xunit.Abstractions;

#if DEBUG

    /// <summary>
    /// The local resource repository tests.
    /// </summary>
    public class LocalResourceRepositoryTests : TestBase
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="LocalResourceRepositoryTests"/> class.
        /// </summary>
        /// <param name="outputHelper">The output helper.</param>
        public LocalResourceRepositoryTests([NotNull] ITestOutputHelper outputHelper)
            : base(outputHelper)
        {
        }

        /// <summary>
        /// Gets all when reading public suffix remote expect read remote.
        /// </summary>
        [Fact]
        public void GetAll_WhenReadingPublicSuffixRemote_ExpectReadRemote()
        {
            // arrange
            var repository = new LocalResourceRepository(this.LoggerFactory);

            // act
            var stopwatch = Stopwatch.StartNew();
            var enumerable = repository.GetAll();
            stopwatch.Stop();

            // assert
            Assert.True(enumerable.Any());
            this.WriteTimeElapsed(stopwatch.ElapsedMilliseconds);
        }
    }

#endif
}