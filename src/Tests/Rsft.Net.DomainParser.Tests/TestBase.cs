﻿// <copyright file="TestBase.cs" company="Rolosoft Ltd">
// (c) 2017  Rolosoft Ltd
// </copyright>

// Copyright 2016 Rolosoft Ltd
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
// http://www.apache.org/licenses/LICENSE-2.0
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
namespace Rsft.Net.DomainParser.Tests
{
    using System;
    using System.IO;
    using JetBrains.Annotations;
    using Microsoft.Extensions.Logging;
    using Serilog;
    using Serilog.Core;
    using Serilog.Events;
    using Xunit.Abstractions;

    /// <summary>
    /// The test base.
    /// </summary>
    public abstract class TestBase
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="TestBase" /> class.
        /// </summary>
        /// <param name="outputHelper">The output helper.</param>
        protected TestBase([NotNull] ITestOutputHelper outputHelper)
        {
            this.OutputHelper = outputHelper;

            var levelSwitch = new LoggingLevelSwitch { MinimumLevel = LogEventLevel.Debug };

            Log.Logger = new LoggerConfiguration()
                .MinimumLevel.ControlledBy(levelSwitch)
                .Enrich.FromLogContext()
                .WriteTo.LiterateConsole(LogEventLevel.Debug)
#if DEBUG
                .WriteTo.File(Path.Combine(Directory.GetCurrentDirectory(), "TestTrace.txt"), LogEventLevel.Debug, shared: true)
#endif
                .CreateLogger();

            this.LoggerFactory = new LoggerFactory().AddSerilog();
        }

        /// <summary>
        /// Gets the output helper.
        /// </summary>
        /// <value>
        /// The output helper.
        /// </value>
        protected ITestOutputHelper OutputHelper { get; }

        /// <summary>
        /// Gets the logger factory.
        /// </summary>
        /// <value>
        /// The logger factory.
        /// </value>
        [NotNull]
        protected ILoggerFactory LoggerFactory { get; }

        /// <summary>
        /// The write time elapsed.
        /// </summary>
        /// <param name="timerElapsed">
        /// The timer elapsed.
        /// </param>
        protected void WriteTimeElapsed(long timerElapsed)
        {
            this.OutputHelper.WriteLine("Elapsed timer: {0}ms", timerElapsed);
        }

        /// <summary>
        /// The write time elapsed.
        /// </summary>
        /// <param name="timerElapsed">
        /// The timer elapsed.
        /// </param>
        protected void WriteTimeElapsed(TimeSpan timerElapsed)
        {
            this.OutputHelper.WriteLine("Elapsed timer: {0}", timerElapsed);
        }
    }
}